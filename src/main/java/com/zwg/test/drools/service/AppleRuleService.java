package com.zwg.test.drools.service;

import com.zwg.test.drools.fact.Apple;
import com.zwg.test.drools.fact.User;
import org.kie.api.KieServices;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.stereotype.Service;

/**
 * 苹果规则服务类
 *
 * @author zwg
 * @date 2018-08-21 20:09
 **/

@Service
public class AppleRuleService {

    /**
     * 获取规则的级别
     * @param apple
     * @return
     */
    public Apple getAppleLevel(Apple apple){

        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.newKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("ksession");
        System.out.println("before :+"+apple.toString());
        kieSession.insert(apple);
        int num = kieSession.fireAllRules();

        kieSession.dispose();

        System.out.println("after fire num:"+num+" "+apple.toString());


        return apple;
    }


}
