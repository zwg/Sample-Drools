package com.zwg.test.drools.service;

import com.zwg.test.drools.fact.Apple;
import com.zwg.test.drools.fact.User;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.internal.utils.KieHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.stereotype.Service;

/**
 * 规则服务
 *
 * @author zwg
 * @date 2018-08-20 15:52
 **/

@Service
public class RuleService {

    private static Logger logger = LoggerFactory.getLogger(RuleService.class);

    @Autowired
    KieSession kieSession;

    @Autowired
    StatelessKieSession statelessKieSession;


    /**
     * 有状态session
     * @param user
     */
    public void annotationOfKieSession(User user, boolean delete){

        FactHandle factHandle = kieSession.insert(user);
        int num = kieSession.fireAllRules();
        if(delete){
            kieSession.delete(factHandle);
        }
        int factSize = kieSession.getFactHandles().size();
        //kieSession.dispose();
        logger.info("annotationOfKieSession fire num:{}, factSize:{}",num, factSize);
    }

    /**
     * 无状态session
     * @param user
     */
    public void annotationOfStatelessKieSession(User user){

       statelessKieSession.execute(user);
        logger.info("annotationOfStatelessKieSession ok");
    }


    /**
     * 规则文件
     * @param user
     */
    public void ruleFile(User user){
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.newKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("ksession");

        kieSession.insert(user);
        int num = kieSession.fireAllRules();
        kieSession.dispose();

        logger.info("ruleFile fire num:"+num);
    }


    /**
     * 规则字符串
     * @param user
     * @return
     */
    public User ruleStr(User user){
        KieHelper helper = new KieHelper();
        //drl同一个package下不能有相同的规则名称，在不同的drl文件也不行
        helper.addContent(getDrl(), ResourceType.DRL);
        helper.addContent(getDrl2(),ResourceType.DRL);
        helper.addContent(getDrl3(),ResourceType.DRL);
        KieSession kSession = helper.build().newKieSession();
        kSession.insert(user);
        logger.info("ruleStr before :{}",user);
        int num = kSession.fireAllRules();
        kSession.dispose();
        logger.info("ruleStr after fire num:{} users:{}",num,user);
        return user;
    }

    /**
     *
     * @return
     */
    private String getDrl(){
        String endStr = "\r\n";
        StringBuilder builder = new StringBuilder();
        builder.append("package rules.test;").append(endStr);
        builder.append("rule \"hello1\"").append(endStr);
        builder.append("when").append(endStr);
        builder.append("").append(endStr);
        builder.append("then").append(endStr);
        builder.append("System.out.println(\"hello1\");").append(endStr);
        builder.append("end");
        return builder.toString();
    }
    private String getDrl2(){
        String endStr = "\r\n";
        StringBuilder builder = new StringBuilder();
        builder.append("package rules.test;").append(endStr);
        builder.append("rule \"hello2\"").append(endStr);
        builder.append("when").append(endStr);
        builder.append("").append(endStr);
        builder.append("then").append(endStr);
        builder.append("System.out.println(\"hello2\");").append(endStr);
        builder.append("end");
        return builder.toString();
    }
    private String getDrl3(){
        String endStr = "\r\n";
        StringBuilder builder = new StringBuilder();
        builder.append("package rules.test;").append(endStr);
        builder.append("rule \"hello3\"").append(endStr);
        builder.append("when").append(endStr);
        builder.append("").append(endStr);
        builder.append("then").append(endStr);
        builder.append("System.out.println(\"hello3\");").append(endStr);
        builder.append("end");
        return builder.toString();
    }

}
