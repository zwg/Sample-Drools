package com.zwg.test.drools.fact;

/**
 * 房间
 *
 * @author zwg
 * @date 2018-08-22 20:59
 **/

public class Room {
    private String name;


    public Room() {
    }

    public Room(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
