package com.zwg.test.drools.fact;

/**
 * 火灾
 *
 * @author zwg
 * @date 2018-08-22 21:01
 **/

public class Fire {

    private Room room;

    public Fire() {
    }

    public Fire(Room room) {
        this.room = room;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
