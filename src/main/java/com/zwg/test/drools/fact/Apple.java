package com.zwg.test.drools.fact;

import java.math.BigDecimal;

/**
 * 苹果对象
 *
 * @author zwg
 * @date 2018-08-21 20:01
 **/

public class Apple {
    private int id;
    private String name;

    private String variety;

    //重量 克
    private int weight;
    //直径 毫米
    private int diameter;


    private int level;

    private BigDecimal price;


    public Apple() {
    }

    public Apple(String name, String variety, int weight, int diameter) {
        this.name = name;
        this.variety = variety;
        this.weight = weight;
        this.diameter = diameter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Apple{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", variety='").append(variety).append('\'');
        sb.append(", weight=").append(weight);
        sb.append(", diameter=").append(diameter);
        sb.append(", level=").append(level);
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
}
