package com.zwg.test.drools.fact;

/**
 * 喷水器
 *
 * @author zwg
 * @date 2018-08-22 21:00
 **/

public class Sprinkler {

    private Room room;
    private boolean on;

    public Sprinkler() {
    }

    public Sprinkler(Room room) {
        this.room = room;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }
}
