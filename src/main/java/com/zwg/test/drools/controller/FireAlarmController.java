package com.zwg.test.drools.controller;

import com.zwg.test.drools.service.FireAlarmRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author zwg
 * @date 2018-08-22 21:22
 **/
@Controller

public class FireAlarmController {


    @Autowired
    private FireAlarmRuleService fireAlarmRuleService;


    @RequestMapping("fileAlarm")
    @ResponseBody
    public String init(@RequestParam(defaultValue = "") String func ){
        String result = func;
        switch (func){
            case "init":
                fireAlarmRuleService.init();
                break;
            case "fire":
                fireAlarmRuleService.fire();
                break;
            case "fireOff":
                fireAlarmRuleService.fireOff();
                break;
            case "status":
                fireAlarmRuleService.status();
                break;
            default:
                result = "please enter func= init fire fireOff status";

        }
        return  result;
    }

}
