package com.zwg.test.drools.service;

import com.zwg.test.drools.fact.Apple;
import com.zwg.test.drools.fact.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;


@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RuleServiceTest {


    @Autowired
    RuleService ruleService;

    @Autowired
    AppleRuleService appleRuleService;

    private User getUser(){
        return new User();
    }

    @Test
    public void annotationOfKieSession(){
        ruleService.annotationOfKieSession(getUser(),false);
    }

    @Test
    public void annotationOfStatelessKieSession(){
        ruleService.annotationOfStatelessKieSession(getUser());
    }

    @Test
    public void ruleFile() {
        //Apple apple = new Apple("a","A",90,45);
        ruleService.ruleFile(getUser());
    }

    @Test
    public void ruleStr() {
        //Apple apple = new Apple("a","A",90,45);
        ruleService.ruleStr(getUser());
    }


    @Test
    public void getAppleLevel(){
        Apple applea = new Apple("a","A",90,45);
        appleRuleService.getAppleLevel(applea);

        Apple appleb = new Apple("b","A",140,63);
        appleRuleService.getAppleLevel(appleb);
    }
}