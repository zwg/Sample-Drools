# Sample-Drools

#### 项目介绍
Drools（JBoss Rules ）具有一个易于访问企业策略、易于调整以及易于管理的开源业务规则引擎，符合业内标准，速度快、效率高。

#### 软件架构
- spring-boot-starter-parent 2.0.0.RELEASE
- Drools  7.9.0.Final

#### 依赖包
- **knowledge-api.jar** - 提供接口和工厂。它清楚地描述用户 API 的职责，还有什么引擎 API。
- **knowledge-internal-api.jar** - 提供内部接口和工厂。
- **drools-core.jar** - 核心引擎，运行时组件。包含 RETE 引擎和 LEAPS 引擎。
- **drools-compiler.jar** - 包含编译器/构建器组件，以获取规则源，并构建可执行规则库。
- **drools-decisiontables.jar** - 决策表编译器组件，在 drools-compiler 组件中使用。支持 Excel 和 CSV 输入格式。

#### 使用方式
+ 配置文件加载  
    在resources目录下创建META-INFO目录，在目录下新建kmodule.xml文件。内容如下：
     ```xml
     <?xml version="1.0" encoding="UTF-8"?>
     <kmodule xmlns="http://jboss.org/kie/6.0.0/kmodule">
         <kbase name="rules" packages="rules.test,rules.apple">
             <ksession name="ksession"/>
         </kbase>
     </kmodule>
     ``` 
     注意事项： kbase的packages必须是drl的文件路径，多个用逗号分隔
     
     
+ Spring boot整合  
    参见 DroolsAutoConfiguration
    
    
#### drl规则

+ 使用方法
    1. 生成drl文件，放在resources目录下
    2. 拼接成字符串，直接使用
+ drl文件示例
    ```
    package test; //package可以随便起名称
    rule "hello"  //规则名称
        when
            //这里如果为空，则表示eval(true)
        then
            System.out.println("hello");
    end
    ```    
    **注意：** drl同一个package下不能有相同的规则名称，在不同的drl文件也不行

#### Session
- KieSession  有状态Session Stateful  使用完需要调用dispose(),否则会内存泄露
   可以参考代码中的起火报警规则

- StatelessKieSession 无状态Session Stateless



#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

